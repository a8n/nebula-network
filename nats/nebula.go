package main

import (
	"encoding/json"
	"github.com/go-yaml/yaml"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclparse"
	"log"
)

type Config struct {
	Pki            Pki                 `hcl:"pki,block" yaml:"pki,omitempty"`
	StaticHostMap  map[string][]string `hcl:"static_host_map,attr" yaml:"static_host_map,omitempty"`
	Lighthouse     Lighthouse          `hcl:"lighthouse,block" yaml:"lighthouse,omitempty"`
	Listen         *Listen             `hcl:"listen,block" yaml:"listen,omitempty"`
	Punchy         *Punchy             `hcl:"punchy,block" yaml:"punchy,omitempty"`
	SSHD           *SSHD               `hcl:"sshd,block" yaml:"sshd,omitempty"`
	Tun            *Tun                `hcl:"tun,block" yaml:"tun,omitempty"`
	Logging        *Logging            `hcl:"logging,block" yaml:"logging,omitempty"`
	Firewall       *Firewall           `hcl:"firewall,block" yaml:"firewall,omitempty"`
	Stats          *Stats              `hcl:"stats,block" yaml:"stats,omitempty"`
	Handshakes     *Handshakes         `hcl:"handshakes,block" yaml:"handshakes,omitempty"`
	LocalRange     string              `hcl:"local_range,optional" yaml:"local_range,omitempty"`
	PreferredRange []string            `hcl:"preferred_ranges,optional" yaml:"preferred_ranges,omitempty"`
	Routines       int                 `hcl:"routines" yaml:"routines,omitempty"`
	Cipher         string              `hcl:"cipher" yaml:"cipher,omitempty"`
}

type Pki struct {
	Ca                string   `hcl:"ca" yaml:"ca,omitempty"`
	Cert              string   `hcl:"cert" yaml:"cert,omitempty"`
	Key               string   `hcl:"key" yaml:"key,omitempty"`
	Blocklist         []string `hcl:"blocklist,optional" yaml:"blocklist,omitempty"`
	DisconnectInvalid bool     `hcl:"disconnect_invalid,optional" yaml:"disconnect_invalid,omitempty"`
}

type Lighthouse struct {
	AmLighthouse      bool                       `hcl:"am_lighthouse,optional" yaml:"am_lighthouse,omitempty"`
	ServeDNS          bool                       `hcl:"serve_dns,optional" yaml:"serve_dns,omitempty"`
	DNS               *DNS                       `hcl:"dns,block" yaml:"dns,omitempty"`
	Interval          int                        `hcl:"interval,optional" yaml:"interval,omitempty"`
	Hosts             []string                   `hcl:"hosts,optional" yaml:"hosts,omitempty"`
	RemoteAllowList   map[string]bool            `hcl:"remote_allow_list,optional" yaml:"remote_allow_list,omitempty"`
	RemoteAllowRanges map[string]map[string]bool `hcl:"remote_allow_ranges,optional" yaml:"remote_allow_ranges,omitempty"`
	// FIXME: The local_allow_list.interfaces map is not supported because HCL2 cannot have multiple types in a map.
	// Currently, only maps are supported.
	LocalAllowList map[string]bool `hcl:"local_allow_list,optional" yaml:"local_allow_list,omitempty"`
}

type DNS struct {
	Host string `hcl:"host,optional" yaml:"host,omitempty"`
	Port int    `hcl:"port,optional" yaml:"port,omitempty"`
}

type Listen struct {
	Host        string `hcl:"host,optional" yaml:"host,omitempty"`
	Port        int    `hcl:"port,optional" yaml:"port"`
	Batch       int    `hcl:"batch,optional" yaml:"batch,omitempty"`
	ReadBuffer  int    `hcl:"read_buffer,optional" yaml:"read_buffer,omitempty"`
	WriteBuffer int    `hcl:"write_buffer,optional" yaml:"write_buffer,omitempty"`
}

type Punchy struct {
	Punch   bool   `hcl:"punch,optional" yaml:"punch,omitempty"`
	Respond bool   `hcl:"respond,optional" yaml:"respond,omitempty"`
	Delay   string `hcl:"delay,optional" yaml:"delay,omitempty"`
}

type SSHD struct {
	Enabled         bool               `hcl:"enabled,optional" yaml:"enabled,omitempty"`
	Listen          string             `hcl:"listen,optional" yaml:"listen,omitempty"`
	HostKey         string             `hcl:"host_key,optional" yaml:"host_key,omitempty"`
	AuthorizedUsers []*AuthorizedUsers `hcl:"authorized_users,block" yaml:"authorized_users,omitempty"`
}

type AuthorizedUsers struct {
	User string   `hcl:"user" yaml:"user,omitempty"`
	Keys []string `hcl:"keys" yaml:"keys,omitempty"`
}

type Tun struct {
	Disabled           bool            `hcl:"disabled,optional" yaml:"disabled,omitempty"`
	Dev                string          `hcl:"dev,optional" yaml:"dev,omitempty"`
	DropLocalBroadcast bool            `hcl:"drop_local_broadcast,optional" yaml:"drop_local_broadcast,omitempty"`
	DropMulticast      bool            `hcl:"drop_multicast,optional" yaml:"drop_multicast,omitempty"`
	TxQueue            int             `hcl:"tx_queue,optional" yaml:"tx_queue,omitempty"`
	Mtu                int             `hcl:"mtu,optional" yaml:"mtu,omitempty"`
	Routes             []*Routes       `hcl:"routes,block" yaml:"routes,omitempty"`
	UnsafeRoutes       []*UnsafeRoutes `hcl:"unsafe_routes,block" yaml:"unsafe_routes,omitempty"`
}

type Routes struct {
	MTU   int    `hcl:"mtu,optional" yaml:"mtu,omitempty"`
	Route string `hcl:"route,optional" yaml:"route,omitempty"`
}

type UnsafeRoutes struct {
	Route  string `hcl:"route,optional" yaml:"route,omitempty"`
	Via    string `hcl:"via,optional" yaml:"via,omitempty"`
	MTU    int    `hcl:"mtu,optional" yaml:"mtu,omitempty"`
	Metric int    `hcl:"metric,optional" yaml:"metric,omitempty"`
}

type Logging struct {
	Level            string `hcl:"level,optional" yaml:"level,omitempty"`
	Format           string `hcl:"format,optional" yaml:"format,omitempty"`
	DisableTimestamp bool   `hcl:"disable_timestamp,optional" yaml:"disable_timestamp,omitempty"`
	TimestampFormat  string `hcl:"timestamp_format,optional" yaml:"timestamp_format,omitempty"`
}

type Stats struct {
	Type              string `hcl:"type" yaml:"type,omitempty"`
	Prefix            string `hcl:"prefix,optional" yaml:"prefix,omitempty"`
	Protocol          string `hcl:"protocol,optional" yaml:"protocol,omitempty"`
	Host              string `hcl:"host,optional" yaml:"host,omitempty"`
	Interval          string `hcl:"interval,optional" yaml:"interval,omitempty"`
	Listen            string `hcl:"listen,optional" yaml:"listen,omitempty"`
	Path              string `hcl:"path,optional" yaml:"path,omitempty"`
	Namespace         string `hcl:"namespace,optional" yaml:"namespace,omitempty"`
	Subsystem         string `hcl:"subsystem,optional" yaml:"subsystem,omitempty"`
	MessageMetrics    bool   `hcl:"message_metrics,optional" yaml:"message_metrics"`
	LighthouseMetrics bool   `hcl:"lighthouse_metrics,optional" yaml:"lighthouse_metrics"`
}

type Handshakes struct {
	TryInterval   string `hcl:"try_interval,optional" yaml:"try_interval,omitempty"`
	Retries       int    `hcl:"retries,optional" yaml:"retries,omitempty"`
	TriggerBuffer int    `hcl:"trigger_buffer,optional" yaml:"trigger_buffer,omitempty"`
}

type Firewall struct {
	Conntrack *Conntrack      `hcl:"conntrack,block" yaml:"conntrack,omitempty"`
	Outbound  []*FirewallRule `hcl:"outbound,block" yaml:"outbound,omitempty"`
	Inbound   []*FirewallRule `hcl:"inbound,block" yaml:"inbound,omitempty"`
}

type Conntrack struct {
	TcpTimeout     string `hcl:"tcp_timeout,optional" yaml:"tcp_timeout,omitempty"`
	UdpTimeout     string `hcl:"udp_timeout,optional" yaml:"udp_timeout,omitempty"`
	DefaultTimeout string `hcl:"default_timeout,optional" yaml:"default_timeout,omitempty"`
	MaxConnections int    `hcl:"max_connections,optional" yaml:"max_connections,omitempty"`
}

type FirewallRule struct {
	Port   string   `hcl:"port" yaml:"port,omitempty"`
	Proto  string   `hcl:"proto" yaml:"proto,omitempty"`
	Host   string   `hcl:"host,optional" yaml:"host,omitempty"`
	Group  string   `hcl:"group,optional" yaml:"group,omitempty"`
	Groups []string `hcl:"groups,optional" yaml:"groups,omitempty"`
	CaName string   `hcl:"ca_name,optional" yaml:"ca_name,omitempty"`
	CaSHA  string   `hcl:"ca_sha,optional" yaml:"ca_sha,omitempty"`
}

func NewNebulaConfig() *Config {
	return &Config{}
}

func (c *Config) Load(filename string) (err error) {
	var diags hcl.Diagnostics

	parser := hclparse.NewParser()
	file, parseDiags := parser.ParseHCLFile(filename)
	diags = append(diags, parseDiags...)
	if diags.HasErrors() {
		log.Fatal(diags)
	}

	decodeDiags := gohcl.DecodeBody(file.Body, nil, c)
	diags = append(diags, decodeDiags...)
	if diags.HasErrors() {
		log.Fatal(diags)
		return diags
	}

	return nil
}

func (c *Config) ToJSON() (js string, err error) {
	var j []byte
	j, err = json.MarshalIndent(c, "", "  ")
	if err != nil {
		log.Printf("Failed to marshal JSON: %v", err)
		return "", err
	}
	return string(j), nil
}

func (c *Config) ToYAML() (ys string, err error) {
	ys = ""
	var y []byte
	y, err = yaml.Marshal(c)
	if err != nil {
		log.Printf("Failed to marshal YAML: %v", err)
		return ys, err
	}
	ys = string(y)
	return ys, nil
}
