# NATS

## Software and Utilities

- nats-server: <https://github.com/nats-io/nats-server>
- nats (cli): <https://github.com/nats-io/natscli>
- nk (cli & library): <https://github.com/nats-io/nkeys>
- nsc (cli): <https://github.com/nats-io/nsc>
- jwt (library): <https://github.com/nats-io/jwt>

## Links

[Implementing a Go function accessible by Script](https://github.com/d5/tengo/issues/277) - Has a link to a working demo
on the playground.

[nsc](https://nats-io.github.io/nsc/) creates NATS operators, accounts, users, and manage their permissions.

[nk](https://github.com/nats-io/nkeys/tree/master/nk) - Nkeys utility program

[Go Examples and CLI Clients](https://github.com/nats-io/go-nats-examples/)

NATS [Authentication](https://docs.nats.io/running-a-nats-service/configuration/securing_nats/auth_intro)

NATS [Authorization](https://docs.nats.io/running-a-nats-service/configuration#authentication-and-authorization)

## Example

Run the server.

```bash
nats-server --trace --config config.conf
```

Subscribe to a channel.

```bash
nats-sub/nats-sub -s nats://localhost:4222 -creds user-a.nk -t '_INBOX.>'
```

Publish a message.

```bash
nats-pub/nats-pub -s nats://localhost:4222 -creds user-b.nk '_INBOX.>' "Here is a message"
```

