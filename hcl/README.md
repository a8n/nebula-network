# Links


[HCL Native Syntax Specification](https://github.com/hashicorp/hcl/blob/main/hclsyntax/spec.md)

[Information Model and Syntax](https://github.com/hashicorp/hcl#information-model-and-syntax)

[HIL](https://github.com/hashicorp/hil) (HashiCorp Interpolation Language) is a lightweight embedded language used
primarily for configuration interpolation. The goal of HIL is to make a simple language for interpolations in the
various configurations of HashiCorp tools.

[HIL docs](https://pkg.go.dev/github.com/hashicorp/hil) - HashiCorp Interpolation Language

[hcldec](https://github.com/hashicorp/hcl/blob/main/cmd/hcldec/README.md) is a command line tool that transforms HCL
input into JSON output using a decoding specification given by the user.

[Introduction to Packer HCL2](https://www.packer.io/guides/hcl)

[packer parser.go](https://github.com/hashicorp/packer/blob/master/hcl2template/parser.go) - Has some in-depth info for
low-level HCL parsing.

Package [gohcl](https://pkg.go.dev/github.com/hashicorp/hcl/v2/gohcl) allows decoding HCL configurations into Go data
structures.

[HCL Config Language Toolkit](https://hcl.readthedocs.io/en/latest/index.html) (`readthedocs.io`) - HCL is a toolkit for
creating structured configuration languages that are both human- and machine-friendly, for use with command-line tools,
servers, etc.


