package main

import (
	"log"
)

func main() {
	var filename = "nebula.hcl"
	var err error

	n := NewNebulaConfig()
	err = n.Load(filename)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	var y string
	y, err = n.ToYAML()
	if err != nil {
		log.Fatalf("yaml error: %v", err)
	}
	log.Printf("YAML:\n%v", y)

	//log.Printf("Struct:\n%+v\n", n)
	//spew.Dump(n)

}
