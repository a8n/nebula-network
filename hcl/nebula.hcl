pki {
  ca                 = <<EOT
-----BEGIN NEBULA CERTIFICATE-----
-----END NEBULA CERTIFICATE-----
EOT
  cert               = <<EOT
-----BEGIN NEBULA CERTIFICATE-----
-----END NEBULA CERTIFICATE-----
EOT
  key                = <<EOT
-----BEGIN NEBULA CERTIFICATE-----
-----END NEBULA CERTIFICATE-----
EOT
  blocklist          = [
    "c99d4e650533b92061b09918e838a5a0a6aaee21eed1d12fd937682865936c72",
  ]
  disconnect_invalid = false
}

static_host_map = {
  "192.168.10.1" = ["100.64.22.11:4242",]
}

lighthouse {
  am_lighthouse     = false
  serve_dns         = false
  dns {
    host = "0.0.0.0"
    port = 53
  }
  interval          = 60
  hosts             = [
    "192.168.1.1"
  ]
  remote_allow_list = {
    # Example to block IPs from this subnet from being used for remote IPs.
    "172.16.0.0/12" = false

    # A more complicated example, allow public IPs but only private IPs from a specific subnet
    "0.0.0.0/0"     = true
    "10.0.0.0/8"    = false
    "10.42.42.0/24" = true
  }

  remote_allow_ranges = {
    # This rule would only allow only private IPs for this VPN range
    "10.42.42.0/24" = {
      "192.168.0.0/16" = true
    }
  }

  local_allow_list = {
    # Example to block tun0 and all docker interfaces.
    # The interfaces map is not supported because HCL2 cannot have multiple types in a map.
    #    interfaces = {
    #      "tun0" = false
    #      "docker.*" = false
    #    }
    # Example to only advertise this subnet to the lighthouse.
    "10.0.0.0/8" = true
  }

}

listen {
  host         = "0.0.0.0"
  port         = 0
  batch        = 64
  read_buffer  = 10485760
  write_buffer = 10485760
}

punchy {
  punch   = true
  respond = true
  delay   = "1s"
}

local_range      = "192.168.1.1/24"
preferred_ranges = ["172.16.0.0/24"]
routines         = 1
cipher           = "chachapoly"

sshd {
  # Toggles the feature
  enabled  = true
  # Host and port to listen on, port 22 is not allowed for your safety
  listen   = "127.0.0.1:2222"
  # A file containing the ssh host private key to use
  # A decent way to generate one: ssh-keygen -t ed25519 -f ssh_host_ed25519_key -N "" < /dev/null
  host_key = "./ssh_host_ed25519_key"
  # A file containing a list of authorized public keys
  authorized_users {
    user = "steeeeve"
    # keys can be an array of strings or single string
    keys = [
      "ssh public key string"
    ]
  }
  authorized_users {
    user = "doug"
    # keys can be an array of strings or single string
    keys = [
      "dougs ssh public key string"
    ]
  }
}

tun {
  disabled             = false
  dev                  = "nebula1"
  drop_local_broadcast = false
  drop_multicast       = false
  tx_queue             = 500
  mtu                  = 1300
  routes {
    mtu   = 8800
    route = "10.0.0.0/16"
  }
  routes {
    mtu   = 8800
    route = "172.0.0.0/16"
  }
  unsafe_routes {
    route  = "172.16.1.0/24"
    via    = "192.168.100.99"
    mtu    = 1300
    metric = 100
  }

}

logging {
  level             = "info"
  format            = "text"
  disable_timestamp = true
  timestamp_format  = "2006-01-02T15:04:05.000Z07:00"
}

stats {
  #  type     = "graphite"
  #  prefix   = "nebula"
  #  protocol = "tcp"
  #  host     = "127.0.0.1:9999"
  #  interval = "10s"

  type      = "prometheus"
  listen    = "127.0.0.1:8080"
  path      = "/metrics"
  namespace = "prometheusns"
  subsystem = "nebula"
  interval  = "10s"

  message_metrics    = false
  lighthouse_metrics = false
}

handshakes {
  try_interval   = "100ms"
  retries        = 20
  trigger_buffer = 64
}

firewall {
  conntrack {
    tcp_timeout     = "12m"
    udp_timeout     = "3m"
    default_timeout = "10m"
    max_connections = 100000
  }

  outbound {
    port  = "any"
    proto = "any"
    host  = "any"
  }

  inbound {
    port  = "any"
    proto = "icmp"
    host  = "any"
  }
  
  inbound {
    port   = "443"
    proto  = "tcp"
    groups = [
      "laptop",
      "home",
    ]
  }

}
