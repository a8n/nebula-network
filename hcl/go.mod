module gitlab.com/a8n/nebula-network/hcl

go 1.16

require (
	github.com/agext/levenshtein v1.2.3 // indirect
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/hashicorp/hcl/v2 v2.11.1
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/zclconf/go-cty v1.10.0 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
