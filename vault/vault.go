package vault

import (
	"errors"
	"github.com/google/uuid"
	"github.com/hashicorp/hcl/v2/hclsimple"
	"github.com/hashicorp/vault/api"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type Vault struct {
	Servers []string `default:""`
	Timeout int      `default:"0"`
	// RoleID and SecretID are UUIDs stored as strings
	RoleID   string `default:""`
	SecretID string `default:""`
	// Vault Client
	client *api.Logical `default:"nil"`
}

func (v Vault) LoadConfig(filename string) error {
	var err error
	// FIXME: HCL doesn't decode UUID automatically.
	// panic: unsuitable DecodeExpression target: no cty.Type for uuid.UUID
	type config struct {
		Servers  []string `hcl:"server_list"`
		Timeout  int      `hcl:"http_timeout" default:"10"`
		RoleID   string   `hcl:"role_id"`
		SecretID string   `hcl:"secret_id"`
	}

	var c config
	var src []byte
	log.Printf("Config: %#v\n", c)
	log.Printf("File: %#v\n", filename)
	src, err = ioutil.ReadFile(filename)
	if err != nil {
		log.Printf("Failed to load configuration: %s", err)
		return err
	}

	// Docs:
	// https://pkg.go.dev/github.com/hashicorp/hcl/v2@v2.3.0/hclsimple
	// https://pkg.go.dev/github.com/hashicorp/hcl2/gohcl
	err = hclsimple.Decode(filename, src, nil, &c)
	if err != nil {
		log.Printf("Failed to load configuration: %s", err)
		return err
	}

	// Check for invalid data
	for _, u := range c.Servers {
		if isValidUrl(u) {
			v.Servers = append(v.Servers, u)
		}
	}
	if c.Timeout >= 0 {
		v.Timeout = c.Timeout
	}

	// Check for valid UUIDs
	_, err = uuid.Parse(c.RoleID)
	if err != nil {
		log.Println("Role ID is not a valid UUID:", c.RoleID)
		return err
	}
	v.RoleID = c.RoleID

	_, err = uuid.Parse(c.SecretID)
	if err != nil {
		log.Println("Secret ID is not a valid UUID:", c.SecretID)
		return err
	}
	v.SecretID = c.SecretID

	return nil
}

// Check for a valid URL. Scheme needs to be HTTPS.
func isValidUrl(s string) bool {
	_, err := url.ParseRequestURI(s)
	if err != nil {
		log.Println("URL is not well formed:", s)
		return false
	}

	u, err := url.Parse(s)
	if err != nil {
		log.Println("URL cannot be parsed:", s)
		return false
	}
	if u.Scheme == "" {
		log.Println("Scheme is empty:", s)
		return false
	}
	if strings.ToLower(u.Scheme) != "https" {
		log.Println("Scheme is insecure:", s)
		return false
	}
	if u.Host == "" {
		log.Println("Host is empty:", s)
		return false
	}

	return true
}

// Create a vault client
func (v Vault) Connect() error {
	var client *api.Client
	var secret *api.Secret
	var auth *api.Auth
	var err error

	for _, addr := range v.Servers {
		log.Printf("Server: %s\n", addr)

		// Create the client
		client, err = api.NewClient(&api.Config{
			Address: addr,
			HttpClient: &http.Client{
				Timeout: time.Duration(v.Timeout) * time.Second,
			},
		})
		if err != nil {
			log.Printf("Failed to connect to vault server %s\n", addr)
			log.Printf("Error: %s\n", err)
			continue
		}

		// Authenticate / Login
		secret, err = client.Logical().Write("auth/approle/login", map[string]interface{}{
			"role_id":   v.RoleID,
			"secret_id": v.SecretID,
		})
		if err != nil {
			log.Printf("Failed to connect login to vault on server %s\n", addr)
			log.Printf("Error: %s\n", err)
			continue
		}

		token, err := secret.TokenID()
		if err != nil {
			log.Printf("Failed to retrieve token or token is nil for server %s\n", addr)
			log.Printf("Error: %s\n", err)
			continue
		}

		// Save the token for future queries
		client.SetToken(token)
		auth = client.Auth()
		if auth == nil {
			log.Printf("Failed to authenticate with server %s\n", addr)
			log.Printf("Error: %s\n", err)
			continue
		}

		v.client = client.Logical()
		return nil
	}

	if client != nil {
		err = errors.New("failed to initialize vault client")
		return err
	}

	v.client = client.Logical()
	return nil
}

// Get the Key/Value secret for the given path
func (v Vault) KvGet(kvPath string) (*api.Secret, error) {
	secret, err := v.client.Read(kvPath)
	if err != nil {
		return nil, err
	}
	if secret != nil {
		return secret, nil
	}
	return nil, errors.New("secret and err are nil")
}
